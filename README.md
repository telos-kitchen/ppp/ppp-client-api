**Project People & Profile Client API**

Enables the usage of the PPP backend services by client applications.

To install the ppp client api run the following command:

`npm i --save @smontero/ppp-client-api`

Access to most of the functionality is done through the PPP object which enables its configuration and provides access to the dfferent API objects:

`import PPP from '@smontero/ppp-client-api`

Before calling any of the PPP APIs, it is required to tell PPP which environment to connect to 'test' or 'prod':

`PPP.configure('prod')`

After this step the public endpoints([getProfiles](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L114) and [searchProfiles](https://gitlab.com/smontero1/ppp-client-api/blob/master/src/service/ProfileApi.js#L279)) can be called.


**Public endpoints**

*  [getProfiles](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L114): Retrieves profiles for specific eosAccounts
*  [searchProfiles](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L279): Enables the search of profiles by eosAccount

*Get Profiles:*

To get the public profiles for specific eosAccounts:

```
const profileApi = PPP.profileApi();
const profiles = await profileApi.getProfiles(['testaccount']);
```
Example payload: 

```
{
      "testaccount": {
        "eosAccount": "testaccount",
        "appId": "__BASE_PROFILE",
        "publicData": {
          "name": "testuser",
          "tags": [
            "football",
            "tennis"
          ],
          "isVerified": 1572649554586,
          "timeZone": "GA",
          "customFields": [
            {
              "value": "Rock Pop",
              "label": "Music"
            },
            {
              "value": "Tennis",
              "label": "Favorite Sport"
            }
          ],
          "content": "Hola!",
          "avatarImage": "testaccount-1573087571312.png",
          "s3Identity": "us-east-1:a9dbf7bf-d1a5-495d-9375-33e78cc2479f"
        }
      }
    }
```

*Search profiles:*

To search profiles by eosaccount:

```
const profileApi = PPP.profileApi();
const profiles = await profileApi.searchProfiles('test');
```
Example payload: 

```
 {
      "items": [
        {
          "eosAccount": "testnm11",
          "publicData": {
            "name": "testuser",
            "isVerified": 1573432491809,
            "tags": [
              "tennis"
            ],
            "timeZone": "Reino Unido",
            "customFields": [],
            "content": "Hi"
          }
        },
        {
          "eosAccount": "testnmb1",
          "publicData": {
            "name": "testuser2",
            "tags": [
              "football",
              "tennis"
            ],
            "isVerified": 1572649554586,
            "timeZone": "GA",
            "customFields": [
              {
                "value": "Rock Pop",
                "label": "Music"
              },
              {
                "value": "Tennis",
                "label": "Favorite Sport"
              }
            ],
            "content": "Hola!",
            "avatarImage": "testnmb1-1573087571312.png",
            "s3Identity": "us-east-1:a9dbf7bf-d1a5-495d-9375-33e78cc2479f"
          }
        }
      ],
      "count": 2
    }

```

**Private endpoints**


*  [getProfile](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L114): Gets signed in user's profile, including private data
*  [register](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L58): Update signed in user's profile
*  [uploadImage](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L74): Upload an image associated to the signed in user
*  [verifySms](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L181): Verify signed in user's sms number
*  [verifyEmail](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L199): Verify signed in user's email
*  [sendMessage](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L162): Send a message
*  [getChats](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L221): Get all conversations the signed in user is engaged in
*  [getMessages](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L251): Get messages between the signed in user and another eosAccount


*Authenticate with the backend:*

To access the private endpoints authentication with the backend is required. 

First we need to set the active user that will be used to authenticate with the backend:

`PPP.setActiveUser(activeUser)`

The active user has to adere to the following interface:

```
{
    async signTransaction(actions, options){}
    async getAccountName()
}
```

To signIn the current user to the backend:

```
const authApi = PPP.authApi()
const session = await authApi.signIn()
```

It can be assumed that the signIn was successful if no exception is thrown. NOTE: For the getProfiles and searchProfiles methods setting the activeUser and signIn are NOT required.

For methods to check if the user has a valid session or to sign out of the backend look at the AuthApi class: [AuthApi](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/AuthApi.js)

Once the user has been signed in, any profile service can be called, profile related services are provided by the profileApi object.

*Get Profile:*

`const profileApi = PPP.profileApi()`

To get the current user profile:

`const profile = await profileApi.getProfile(fetchType)`

A fetch type can be specified to retrieve only the base profile data or only the app data or both. Please view [@smontero/ppp-common ProfileFetchTypes class](https://gitlab.com/telos-kitchen/ppp-common/blob/master/src/const/ProfileFetchTypes.js)

*Register:*

To update or register a new user profile:

`const profile = await profileApi.register(data)`

Example data object:

`{
  "emailAddress": "",
  "smsNumber": "",
  "commPref": "EMAIL",
  "publicData": {
    "firstName": "Jhon",
    "lastName": "Doe",
    "countryCode": "MX",
    "profileImage": "testuser1111-1573251914639.png",
    "s3Identity": "us-east-1:ee696abc-cd8e-4ad1-8cd3-61653a39d863",
    "hobbies": [
      "tennis"
    ],
    "bio": "Hi, this is a test",
    "customFields": [
      {
        "label": "Music",
        "value": "Rock"
      }
    ]
  }
}`

The data object has 5 main sections:

1. Root fields, which are the communication related fields: emailAddress, smsNumber, commPref look at @smontero/ppp-commnon -> [RootFields](https://gitlab.com/telos-kitchen/ppp-common/blob/master/src/const/RootFields.js) and  @smontero/ppp-commnon -> [CommMethods](https://gitlab.com/telos-kitchen/ppp-common/blob/master/src/const/CommMethods.js)
2. Public Data, non app specific public profile data look at @smontero/ppp-commnon -> [PublicFields](https://gitlab.com/telos-kitchen/ppp-common/blob/master/src/const/PublicFields.js), it is poosible to add fields other than the ones describe in the ppp-common PublicFields class.
3. Private Data, non app specific private data, that only the owner can see
4. App Public Data, app specific public profile data
3. App Private Data, app specific private data, that only the owner can see

For more details on the structure of the data object look the register method docs: [ProfileApi](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js)

*Upload Image:*

To upload an image associated to the signed in user account: 

`const fileName = await profileApi.uploadImage(file)`

The image file is passed in as parameter, the name of the file in the store is returned.


*Verfiy Email and SMS:*


To verify email and sms:

`await profileApi.verifyEmail(verficationCode)`

`await profileApi.verifySms(verficationCode)`

It can be assumed that verification was successful if no exception is thrown.


*Send Message:*

To send a message:

`await profileApi.sendMessage(eosAccount, message)`

The destination eosAccount and the message are passed in as parameters.

It can be assumed that verification was successful if no exception is thrown.


*Get Chats:*

Retrieves the chats a specific user is involved in, returns from the most recent chat to the least recent, it can also search for a chat with specific counter party

`const chats = await profileApi.getChats(search, limit, lastEvaluatedKey)`

For more information on the parameters please review the docs of the method [getChats](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L221)

Example payload: 


```
{
  "items": [
    {
      "subject": "Hi...",
      "isSender": true,
      "counterParty": {
        "firstName": "Jose Maria",
        "lastName": "Gayosso",
        "profileImage": "jmgayosso155-1573093624130.png",
        "s3Identity": "us-east-1:a759e5af-054b-4418-84ad-5b9fcf43447f"
      },
      "eosAccount": "sebastianmb1",
      "counterPartyAccount": "jmgayosso155",
      "message": "Hi",
      "sentAt": 1576192208645
    },
    {
      "subject": "Como estas?...",
      "isSender": true,
      "counterParty": {
        "firstName": "Sebastian",
        "lastName": "Montero",
        "profileImage": null,
        "s3Identity": null
      },
      "eosAccount": "sebastianmb1",
      "message": "Como estas?",
      "counterPartyAccount": "sebastianm11",
      "sentAt": 1576192025018
    }
  ],
  "count": 2
}
```



*Get Messages:*

Retrieves the messages between the current user and the user specified in the eosAccount parameter, returns from the most recent message to the least recent

`const messages = await profileApi.getMessages(search, limit, lastEvaluatedKey)`

For more information on the parameters please review the docs of the method [getMessages](https://gitlab.com/telos-kitchen/ppp-client-api/blob/master/src/service/ProfileApi.js#L251)

Example payload: 

```
{
  "items": [
    {
      "subject": "Bien!...",
      "senderAccount": "sebastianm11",
      "messageKey": "e2a3ec40-1d34-11ea-a8ec-73978b79e52c",
      "eosAccount": "sebastianmb1",
      "message": "Bien!",
      "sentAt": 1576192356612
    },
    {
      "subject": "Como estas?...",
      "senderAccount": "sebastianmb1",
      "messageKey": "1cfeb1a0-1d34-11ea-a8ec-73978b79e52c",
      "eosAccount": "sebastianm11",
      "message": "Como estas?",
      "sentAt": 1576192025018
    },
    {
      "subject": "Hola...",
      "senderAccount": "sebastianmb1",
      "messageKey": "1899a9d0-1d34-11ea-a8ec-73978b79e52c",
      "eosAccount": "sebastianm11",
      "message": "Hola",
      "sentAt": 1576192017645
    }
  ],
  "count": 3
}
```


