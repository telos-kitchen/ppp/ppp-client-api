import PPP from './PPP';
import { DeepError } from './error';

export default PPP;

export {
  PPP,
  DeepError,
};